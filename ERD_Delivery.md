Структура базы данных "Служба доставки":

- включая связь один-ко-многим;
- включая связь многие-ко-многим;
- все отношения приведены к 3НФ.

ERD Служба доставки:

- диаграмма 
https://dbdesigner.page.link/DasRkqPNuUS3dBew9

- excel с пояснениями (+ 1НФ, 3НФ) 
https://docs.google.com/spreadsheets/d/1fsPV0zRiMNzdU_qGght7edcr1h59jCbeKbuoLEu6ErY/edit#gid=0
